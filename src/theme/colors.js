export default {
 
	primaryDark: '#000',
	primary: '#fff',
	tabBarBackground: '#fff',
	tabBarIconInactive: '#969696',
	tabBarIconActive: '#E5001D',


	appbarTint: '#000',


	secondaryLight: '#000',
	secondary: '#000',
	secondaryDark: '#000',
	 


	disabled: 'hsl(208, 8%, 90%)',
	disabledDark: 'hsl(208, 8%, 60%)',
	 
	transparent: 'transparent',
	
	
	background: '#f5f8fe',
	 
	surface: '#fff',
	
	
	border: '#dcd6d5',
	 
	success: '#52c41a',
	 
	error: '#ff190c',
	warning: '#ddd',
	information: '#333',
	
	
	titleText: '#202020',
	 
	bodyText: '#737373',
	 
	captionText: '#8c8c8c',
	black: '#000000',
	white: '#ffffff',
};
