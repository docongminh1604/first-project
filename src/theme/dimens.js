import { Dimensions, Platform } from 'react-native';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

export default {
	/**
	 * App level constants
	 */
	WINDOW_WIDTH: screenWidth,
	WINDOW_HEIGHT: screenHeight,
	// headerButtonSize: 23,
	borderRadius: 4,
	defaultButtonWidth: screenWidth * 0.7,
	defaultButtonHeight: screenHeight * 0.06,
	defaultInputBoxHeight: 50,
	productListItemInBetweenSpace: 1,
	productListItemImageHeight: 200,
 
	orderImageWidth: 100,
	orderImageHeight: 100,
 
	checkouSectionHeaderHeight: 50,

	toolBarHeight: screenHeight * 0.06,
	toolBarBtnWidth: 45,
	toolBarButtonPadding: 9,
	
};
