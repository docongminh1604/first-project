import React, { useContext, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { 
	SafeAreaView, 
	StyleSheet,
	View,
	Text
} from 'react-native';
import { ThemeContext } from '../../theme';

const Search = ({

}) => {
    const theme = useContext(ThemeContext);

	useEffect(() => {

	}, []);
	
	return (
		<SafeAreaView style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
			<View>
				<Text>Search</Text>
			</View>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({

});

Search.propTypes = {};

Search.defaultProps = {};

const mapStateToProps = ({ }) => {
	return {

	};
};

export default connect(mapStateToProps, {

})(Search);
