import React, { useContext, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { 
	SafeAreaView, 
	StyleSheet,
	View,
	Text,
	Alert
} from 'react-native';
import { ThemeContext } from '../../theme';
import { Header } from '../../common';
import Images from '../../../assets/img';
import NavigationService from '../../navigation/NavigationService'

const Home = ({

}) => {
    const theme = useContext(ThemeContext);

	useEffect(() => {

	}, []);

	const onPressLeftIcon = () => {
		Alert.alert('onPressLeftIcon')
	}
	const onPressContentIcon = () => {
		Alert.alert('onPressContentIcon')
	}
	const onPressRightIcon = () => {
		Alert.alert('onPressRightIcon')
	}
	
	return (
		<SafeAreaView>
			<Header
				imageNearLeft= {Images.ic_header_back}
				onPressNearLeft= {onPressLeftIcon}
				contentTitle= 'First Title'
				onPressContentTitle= {onPressContentIcon}
				iconNearRight= 'bell-o'
				onPressNearRight= {onPressRightIcon}
			/>
			<View>
				<Text>Home</Text>
			</View>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({

});

Home.propTypes = {};

Home.defaultProps = {};

const mapStateToProps = ({ }) => {
	return {

	};
};

export default connect(mapStateToProps, {

})(Home);
