import React, { useContext, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { 
	SafeAreaView, 
	StyleSheet,
	View,
	Text
} from 'react-native';
import { ThemeContext } from '../../theme';

const Menu = ({

}) => {
    const theme = useContext(ThemeContext);

	useEffect(() => {

	}, []);
	
	return (
		<SafeAreaView style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
			<View>
				<Text>Menu</Text>
			</View>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({

});

Menu.propTypes = {};

Menu.defaultProps = {};

const mapStateToProps = ({ }) => {
	return {

	};
};

export default connect(mapStateToProps, {

})(Menu);
