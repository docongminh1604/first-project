import React, { useContext, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import { 
	SafeAreaView, 
	StyleSheet,
	View,
	Text
} from 'react-native';
import { ThemeContext } from '../../theme';

const Filter = ({

}) => {
    const theme = useContext(ThemeContext);

	useEffect(() => {

	}, []);
	
	return (
		<SafeAreaView style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
			<View>
				<Text>Filter</Text>
			</View>
		</SafeAreaView>
	);
};

const styles = StyleSheet.create({

});

Filter.propTypes = {};

Filter.defaultProps = {};

const mapStateToProps = ({ }) => {
	return {

	};
};

export default connect(mapStateToProps, {

})(Filter);
