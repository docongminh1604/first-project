import { combineReducers } from 'redux';
import NopCommerce from './NopCommerce';

export default combineReducers({
	nopCommerce: NopCommerce,
});
