
import {
	NOP_COMMERCE_GET_LANGUAGE,
} from '../actions/types';

const INITIAL_STATE = {
	nopCommerce: null,
 
	states: null,
	 
	// language: 'en'
	language: 'vi'
};

export default (state = INITIAL_STATE, action) => {
 
	switch (action.type) {
		case NOP_COMMERCE_GET_LANGUAGE: {  
			return { ...state, language: action.payload };
		}
		default:
			return state;
	}
};
