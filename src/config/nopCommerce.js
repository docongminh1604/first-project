
export const nopCommerceOptions = {
	url: '',

	store: 'default',
	authentication: {
		integration: {
			access_token: ""
		},
	},
};