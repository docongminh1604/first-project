import AsyncStorage from '@react-native-community/async-storage';
import {
	initNopCommerce, 
	setLanguage,
} from '../actions';
import i18n from 'i18n-js';

import NavigationService from '../navigation/NavigationService';


export const onAppStart = async (store) => {
	store.dispatch(initNopCommerce());

	let locale = await AsyncStorage.getItem('USER_LANGUAGE_PICKED');

	locale = locale ? locale : 'vi';

	i18n.locale = locale;

	store.dispatch(setLanguage(locale));

};
