export function randomNumber(min = 0, max: 1000) {
	return min + Math.random() * (max - min);
}

export function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' đ';
}