import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';
import * as routes from './routes';

import FastImage from 'react-native-fast-image';


import { theme } from '../theme';
import Images from '../../assets/img';
// Account
import AccountScreen from '../components/account/Account';
// Filter
import FilterScreen from '../components/filter/Filter';
// Home
import HomeScreen from '../components/home/Home';
// Category
import CategoryScreen from '../components/category/Category';
// News
import NewsScreen from '../components/news/News';
// Search
import SearchScreen from '../components/search/Search';
// Menu
import SideMenu from '../components/SideMenu/Menu';

const AccountStack = createStackNavigator({
	[routes.NAVIGATION_ACCOUNT_SCREEN_PATH]: AccountScreen,
}, {
	headerMode: 'none',
});
const FilterStack = createStackNavigator({
	[routes.NAVIGATION_FILTER_SCREEN_PATH]: FilterScreen,
	
}, {
	headerMode: 'none',
	// transitionConfig: TransitionConfig,
	defaultNavigationOptions: {
		...TransitionPresets.SlideFromRightIOS,
	},
});
const HomeStack = createStackNavigator({
	[routes.NAVIGATION_HOME_SCREEN_PATH]: HomeScreen,
	
}, {
	headerMode: '',
	defaultNavigationOptions: {
		...TransitionPresets.SlideFromRightIOS,
	},
});
const CategoryStack = createStackNavigator({
	[routes.NAVIGATION_CATEGORY_SCREEN_PATH]: CategoryScreen,
}, {
	headerMode: 'none',
	defaultNavigationOptions: {
		...TransitionPresets.SlideFromRightIOS,
	},
	
});
const NewsStack = createStackNavigator({
	[routes.NAVIGATION_NEWS_SCREEN_PATH]: NewsScreen,
}, {
	headerMode: 'none',
	defaultNavigationOptions: {
		...TransitionPresets.SlideFromRightIOS,
	},
});


const MainAppNavigator = createBottomTabNavigator({
	[routes.NAVIGATION_CATEGORY_STACK_PATH]: {
		screen: CategoryStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => {
				return (
					<FastImage resizeMode="contain"  
						tintColor={tintColor}
						style={{ width: 22, height: 22 }}
						source={Images.ic_bottom_category} />
				)
			},
		}),
		defaultNavigationOptions: {
			...TransitionPresets.SlideFromRightIOS,
		},
	},
	[routes.NAVIGATION_FILTER_STACK_PATH]: {
		screen: FilterStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => {
				return <FastImage resizeMode="contain"  
				tintColor={tintColor}
					style={{ width: 22, height: 22, tintColor: tintColor }}
					source={Images.ic_bottom_account} />;
			},
		}),
		defaultNavigationOptions: {
			...TransitionPresets.SlideFromRightIOS,
		},
	},
	[routes.NAVIGATION_HOME_STACK_PATH]: {
		screen: HomeStack,
		navigationOptions: () => ({
			title: ``,
			tabBarIcon: ({ tintColor }) => {
				return (
					<FastImage resizeMode="contain" 
					style={{ width: 22, height: 22, tintColor: 'blue', marginTop: 15 }}
					source={tintColor !== 'black' ? Images.ic_bottom_home : Images.ic_bottom_home1} />
				);
			},
		}),
		defaultNavigationOptions: {
			...TransitionPresets.SlideFromRightIOS,
		},
	},
	[routes.NAVIGATION_NEWS_STACK_PATH]: {
		screen: NewsStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => {
				return <FastImage resizeMode="contain" 
					tintColor={tintColor}
				 	style={{ width: 22, height: 22, tintColor: tintColor }}
					source={Images.ic_bottom_notifycation} />;
			},
		}),
	},
	[routes.NAVIGATION_ACCOUNT_STACK_PATH]: {
		screen: AccountStack,
		navigationOptions: () => ({
			tabBarIcon: ({ tintColor }) => {
				return <FastImage resizeMode="contain"  
				tintColor={tintColor}
					style={{ width: 22, height: 22, tintColor: tintColor }}
					source={Images.ic_bottom_account} />;
			},
		}),
		defaultNavigationOptions: {
			...TransitionPresets.SlideFromRightIOS,
		},
	},

}, {

	initialRouteName: routes.NAVIGATION_HOME_STACK_PATH,
	tabBarOptions: {
		//show label text
		// showLabel: false,
		showLabel: true,
		
		// activeTintColor: theme.colors.tabBarIconActive,
		activeTintColor: 'black',
		
		inactiveTintColor: theme.colors.tabBarIconInactive,
		activeBackgroundColor: theme.colors.tabBarBackground,
		inactiveBackgroundColor: theme.colors.tabBarBackground,
		labelStyle: {
			fontFamily: theme.fontFamily,
			fontSize: 12,
		},
		
		style: {
			backgroundColor: 'white',
			borderTopWidth: 0,

			// shadowColor: (Platform.OS === 'ios') ? '#ddd' : '#000000',
			// shadowOffset: { width: 5, height: -5 },
			// shadowOpacity: 10,
			// shadowRadius: 6,
			// elevation: 10,

			// borderWidth: 1,

			// height: theme.dimens.defaultButtonHeight,
		},
		style:{
			height: theme.dimens.defaultButtonHeight,
		}
	},
},
);

const DemoLayoutNavigator = createDrawerNavigator({
	MainAppNavigator: MainAppNavigator,
}, {
	contentComponent: SideMenu,
	drawerWidth: 300,
	drawerType: 'slide',
	drawerPosition: 'left'
	// overlayColor: 1,
});

// const MainSwitch = createSwitchNavigator({
// 	[routes.NAVIGATION_INSIDE_NAVIGATOR]: DemoLayoutNavigator,
// 	[routes.NAVIGATION_AUTH_LOADING_STARTUP_SWITCH]: AuthLoadingStartUp,
// 	[routes.NAVIGATION_OUTSIDE_NAVIGATOR]: AuthStack,
// });

const MainSwitch = createSwitchNavigator(
	{
		[routes.NAVIGATION_INSIDE_NAVIGATOR]: DemoLayoutNavigator,
	},
	{
        mode: 'modal',
        headerMode: 'none',
    },
);

export const Navigator = createAppContainer(MainSwitch);
