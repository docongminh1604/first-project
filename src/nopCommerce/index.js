import _ from 'lodash';
import admin from './lib/admin';
import guest from './lib/guest';
import customer from './lib/customer';
import { ADMIN_TYPE, CUSTOMER_TYPE, ADMIN_AUTH_TYPE } from './types';
import { logError } from '../helper/logger';

const defaultOptions = {
	url: null,
	authentication: {
		integration: {
			access_token: undefined,
		},
	},
};

class NopCommerce {

}

export const nopCommerce = new NopCommerce();
