import AsyncStorage from '@react-native-community/async-storage';
import { nopCommerce } from '../nopCommerce';
import { nopCommerceOptions } from '../config/nopCommerce';
import {
	NOP_COMMERCE_GET_LANGUAGE,
} from './types';
import { logError } from '../helper/logger';

export const initNopCommerce = () => {
	return async (dispatch) => {
		try {
			
		} catch (error) {
			logError(error);
		}
	};
};

export const setLanguage = (code) => ({
	type: NOP_COMMERCE_GET_LANGUAGE,
	payload: code,
});