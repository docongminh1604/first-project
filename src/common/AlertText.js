import React, { useContext, Children } from 'react';
import { StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { ThemeContext } from '../theme';
import { Text } from './Text';

// Possible value for prop "type" for Text
const WARNING = 'warning';
const ERROR = 'error';
const SUCCESS = 'success';
const INFORMATION = 'information';


const AlertText = ({
	opacity,
	style,
	alertType,
	children,
	...props

}) => {
	const theme = useContext(ThemeContext);
	var bagColor = theme.colors.error;
	switch (alertType) {
		case WARNING:
			bagColor = theme.colors.warning;
			break;
		case ERROR:
			bagColor = theme.colors.error;
			break;
		case SUCCESS:
			bagColor = theme.colors.success;
			break;
		case INFORMATION:
			bagColor = theme.colors.information;
			break;
	}

	if (opacity) {
		bagColor = bagColor + opacity;
	}

	return (
		<Text
			{...props}
			style={StyleSheet.flatten([styles.text(theme, bagColor), style])} >
			{children}
		</Text>
	);
};



const styles = {
	text: (theme, bgColor) => ({
		padding: 3,
		borderRadius: 4,
		backgroundColor: bgColor,
		color: theme.colors.white,
		textAlign: 'center',
		marginTop: theme.spacing.small,
	}),
	error: theme => ({

	}),
};

Text.propTypes = {
	alertType: PropTypes.oneOf([WARNING, ERROR, SUCCESS, INFORMATION]),

};

Text.defaultProps = {
	alertType: INFORMATION,
};

export { AlertText };
