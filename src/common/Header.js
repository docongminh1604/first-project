import React, {useContext} from "react";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  TouchableWithoutFeedback,
  BackHandler,
  Image,
  Dimensions,
  ViewPropTypes
} from "react-native";
import { ThemeContext } from "../theme";
import FastImage from 'react-native-fast-image';
import PropTypes from 'prop-types';
import { getPaddingBonus, elevationShadowHeaderStyle } from "./Functions";
import Icon from "react-native-vector-icons/FontAwesome";
import Ripple from 'react-native-material-ripple';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const Header = ({
    onPressNearLeft,
    onPressNearLeft1,
    onPressNearLeft2,
    iconNearLeft,
    iconNearLeft1,
    iconNearLeft2,
    imageNearLeft,
    imageNearLeft1,
    imageNearLeft2,

    contentTitle,
    onPressContentTitle,
    imageTitle,

    onPressImageTitle,
    onPressNearRight,
    onPressNearRight1,
    onPressNearRight2,
    iconNearRight,
    iconNearRight1,
    iconNearRight2,
    imageNearRight,
    imageNearRight1,
    imageNearRight2,
}) => {
    const theme = useContext(ThemeContext);
    
    return (
        <View style={styles.toolBar(theme)}>
            {/* {console.log('imageNearLeft',imageNearLeft)}
            {console.log('onPressNearLeft',onPressNearLeft)}
            {console.log('contentTitle',contentTitle)}
            {console.log('iconNearRight',iconNearRight)} */}
            {(imageNearLeft !== null) ?
                <View>
                    <Ripple
                        onPress={onPressNearLeft}
                        style={styles.toolbarBtnWrapperStyle(theme)}
                        rippleColor={"rgb(255, 255, 255)"}>
                        <FastImage
                            resizeMode="contain"
                            style={[styles.toolbarBtnImgStyle(theme), {}]}
                            source={imageNearLeft}
                        />
                    </Ripple>
                </View>
                :
                <View style={{ flexDirection: 'row' }}>
                    <Ripple
                        onPress={onPressNearLeft1}
                        style={styles.toolbarBtnWrapperStyle(theme)}
                        rippleColor={"rgb(255, 255, 255)"}>
                        <FastImage
                            resizeMode="contain"
                            style={styles.toolbarBtnImgStyle(theme)}
                            source={imageNearLeft1}
                        />
                    </Ripple>
                    <Ripple
                        onPress={onPressNearLeft2}
                        style={styles.toolbarBtnWrapperStyle(theme)}
                        rippleColor={"rgb(255, 255, 255)"}>
                        <FastImage
                            resizeMode="contain"
                            style={styles.toolbarBtnImgStyle(theme)}
                            source={imageNearLeft2}
                        />
                    </Ripple>
                </View>
            }
            {contentTitle !== '' ?
                <TouchableOpacity onPress={onPressContentTitle}>
                    <Text bold style={[styles.toolBarTitle, {marginLeft: 0} ]} type="heading"> 
                        {contentTitle} 
                    </Text>
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={onPressImageTitle}>
                    <FastImage
                        resizeMode="contain"
                        style={{ width: viewportWidth * 0.5, height: 30 }}
                        source={imageTitle}
                    />
                </TouchableOpacity>
            }
            {(iconNearRight !== '') ?
                <View>
                    <Icon name={iconNearRight} size={25} 
                        style={[styles.toolbarBtnWrapperStyle(theme), {color: 'black'}]} 
                        onPress={onPressNearRight}
                    />
                </View>
                :
                <View style={{ flexDirection: 'row' }}>	
                    <Icon name={iconNearRight1} size={25} 
                        style={styles.toolbarBtnWrapperStyle(theme)} 
                        onPress={onPressNearRight1}
                    />
                    <Icon name={iconNearRight2} size={25} 
                        style={styles.toolbarBtnWrapperStyle(theme)} 
                        onPress={onPressNearRight2}
                    />
                </View>
            }
        </View>
				
    );
}

const styles = {
	toolBar: theme => ({
		height: 50,
		// backgroundColor: theme.colors.background,
		backgroundColor: 'white',
		justifyContent: "space-between",
		alignItems: 'center',
		flexDirection: 'row',
		width: viewportWidth,

		shadowColor: (Platform.OS === 'ios') ? '#ddd' : '#000000',
		shadowOffset: {width: 5, height: 8},
		shadowOpacity: 8,
		shadowRadius: 6,
		elevation: 4,
	}),
	toolbarBtnWrapperStyle: theme => ({
		padding: theme.dimens.toolBarButtonPadding,
		height: theme.dimens.toolBarBtnWidth,
		width: theme.dimens.toolBarBtnWidth,
	}),
    toolbarBtnImgStyle: () => ({ flex: 1 }),
	toolBarTitle: {
		fontSize: 24,
		textAlign: "center",
		// color: "#202020" 
	},
};

Header.propTypes = {
	style: ViewPropTypes.style,
	disabled: PropTypes.bool,

    onPressNearRight: PropTypes.func,
    onPressNearRight1: PropTypes.func,
    onPressNearRight2: PropTypes.func,
    contentTitle: PropTypes.string,
    onPressContentTitle: PropTypes.func,
    onPressImageTitle: PropTypes.func,
    onPressNearLeft: PropTypes.func,
    onPressNearLeft1: PropTypes.func,
    onPressNearLeft2: PropTypes.func,
};

Header.defaultProps = {
	style: {},

    iconNearRight: '',
    iconNearRight1: '',
    iconNearRight2: '',
    imageNearRight: null,
    imageNearRight1: null,
    imageNearRight2: null,
    contentTitle: '',
    imageTitle: null,
    iconNearLeft: '',
    iconNearLeft1: '',
    iconNearLeft2: '',
    imageNearLeft: null,
    imageNearLeft1: null,
    imageNearLeft2: null
};

export { Header };
