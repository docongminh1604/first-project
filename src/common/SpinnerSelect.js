import React, { useState, useContext } from 'react';
import { View, ViewPropTypes, Image } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import PropTypes from 'prop-types';
import { Input } from './Input';
import { ThemeContext } from '../theme';
import Images from '../../assets/img'
import FastImage from 'react-native-fast-image';
const SpinnerSelect = ({
	data,
	disabled,
	label,
	onChange,
	attribute,
	style,
	inputStyle,
	inputContainer,
	value,
	styleIcon
}) => {
	// const [value, setValue] = useState('');
	const theme = useContext(ThemeContext);
	const _onChange = (value, index) => {
		// setValue(attribute === 'CurrencyCode' ? option.label : `${label} : ${option.label}`);

		if (onChange) {
			onChange(attribute, value);
		}
	};

	return (
		<View style={[styles.dropdownWrapper(theme), Platform.OS === 'ios' ? styles.dropDownWrapperIos : {}, style]}>
			<RNPickerSelect
				style={styles.dropdown(theme)}
				// onValueChange={(value, index) => { }} 
				onValueChange={_onChange}
				key={data.length}
				value={value}
				placeholder={label}
				items={data}
				Icon={() => {
					return <FastImage
						resizeMode="contain"
						style={[styles.downArrowStyle, styleIcon]}
						source={Images.ic_down}
					/>
				}}
			/>
		</View>
	);
};

// TODO: add style for disabled element
const styles = {


	dropdown: theme => ({
		height: theme.dimens.defaultInputBoxHeight,
		justifyContent: 'center',
		// alignItems: 'center',
		paddingStart: 10,
		// borderColor: "#ddd",
		inputAndroid: {
			height: theme.dimens.defaultInputBoxHeight,
			// height: 30,
			backgroundColor: theme.colors.white
		},
		inputIOSContainer: {
			height: theme.dimens.defaultInputBoxHeight,
			justifyContent: 'center',
			paddingLeft: 8,
			backgroundColor: theme.colors.white,
			borderRadius: 4
			// padding: 30,
		}
	}),
	dropdownWrapper: theme => ({

		// alignSelf: 'flex-end',
		// width: 120,
		// marginTop: 10,
		// right: 8,
		// flex: 1,
		borderWidth: 1,
		borderRadius: 4,
		// marginStart: 10,
		borderColor: theme.colors.border,
		// alignItems: 'center'
		// backgroundColor: '#fff',
		// marginTop: 20,

		// backgroundColor: "green"
	}),
	inputStyle: {
		textAlign: 'center',
	},
	downArrowStyle: {
		height: 15,
		width: 15,
		right: 10,
		top: 15
	}
};

SpinnerSelect.propTypes = {
	data: PropTypes.arrayOf(PropTypes.shape({
		value: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
		]).isRequired,
		label: PropTypes.string,
	})).isRequired,
	// label: PropTypes.string.isRequired,
	attribute: PropTypes.string,
	onChange: PropTypes.func,
	disabled: PropTypes.bool,
	style: ViewPropTypes.style,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number,
	]),
};

SpinnerSelect.defaultProps = {
	value: null,
	disabled: false,
	onChange: null,
	attribute: '',
	style: {},
};

export { SpinnerSelect };
