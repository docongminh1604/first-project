import React, { useContext, useState } from 'react';
import { 
    View, 
    ViewPropTypes, 
    ActivityIndicator, 
    Dimensions, 
    Text, 
    Modal,
    Pressable
} from 'react-native';
import PropTypes from 'prop-types';
import { ThemeContext } from '../theme';
import { TouchableOpacity } from 'react-native-gesture-handler';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

const LoadingView = ({
	size,
	style,
}) => {
	const theme = useContext(ThemeContext);

    const [modalVisible, setModalVisible] = useState(true);
	return (
		// <View style={[styles.spinnerStyle, style]}>
		// 	<ActivityIndicator
		// 		size={size}
		// 		color={theme.colors.secondary}
		// 	/>
        //     <Text style={{ fontWeight: 'bold', fontSize: 20, textAlign: 'center' }}>Đang xử lý dữ liệu</Text>
		// </View>

        <Modal
            transparent={true}
            animationType={'fade'}
            visible={modalVisible}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator
                        size={size}
                        color={theme.colors.secondary}
                        style={{ marginTop: 10 }}
                    />
                    <Text style={[styles.textLoading, { color: 'black' }]}>Đang xử lý dữ liệu</Text>
                    <Pressable 
                        style={{ marginTop: 10, marginBottom: 10, borderWidth: 0.5, borderRadius: 6 }} 
                        onPress={ () => setModalVisible(false) }
                    >
                        <Text style={[styles.textLoading, { color: 'black', padding: 5}]}>Cancel</Text>
                    </Pressable>
                </View>
            </View>
        </Modal>
	);
};

const styles = {
	spinnerStyle: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
        height: viewportHeight,
        width: viewportWidth
	},

    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        opacity: 0.7,
    },
    activityIndicatorWrapper: {
        backgroundColor: '#dcdcdc',
        height: 125,
        width: 200,
        opacity: 0.7,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    textLoading: {
        fontWeight: 'bold', 
        fontSize: 20, 
        textAlign: 'center'
    }
};

LoadingView.propTypes = {
	size: PropTypes.oneOf(['large', 'small']),
	style: ViewPropTypes.style,
};

LoadingView.defaultProps = {
	size: 'large',
	style: {},
};

export { LoadingView };
