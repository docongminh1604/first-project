import React, {useContext} from 'react';
import {StyleSheet, TouchableOpacity, ViewPropTypes} from 'react-native';
import PropTypes from 'prop-types';
import {Text} from './Text';
import {ThemeContext} from '../theme';
import Ripple from 'react-native-material-ripple';

const Button = ({
					onPress,
					children,
					style,
					disabled,
					buttonColor,
					curved,
					buttonTextStyle,
				}) => {
	const theme = useContext(ThemeContext);
	const {buttonStyle, buttonTitle} = styles;
	return (
		<Ripple
			rippleColor={"black"}
			opacity={disabled ? 0.5 : 1}

			onPress={onPress}
			style={[buttonStyle(theme, disabled, buttonColor, curved), style]}
			disabled={disabled}
		>
			<Text style={[buttonTitle(theme, disabled), buttonTextStyle]}>
				{children}
			</Text>
		</Ripple>
	);
};

const getButton = (theme, disabled, buttonColor, curved) => {
	let style = {
		// width: theme.dimens.defaultButtonWidth,
		height: theme.dimens.defaultButtonHeight,
		justifyContent: 'center',

		// borderRadius: 20,
	};
	switch (buttonColor) {
		case 'green':
			style['backgroundColor'] = '#87c441';
			break;
		case 'red':
			style['backgroundColor'] = '#ff0000';
			break;
		case 'blue':
			style['backgroundColor'] = '#5eb7ef';
			break;

		default:
			style['backgroundColor'] = '#171717';
	}


	switch (curved) {
		case 'none':
			style['borderRadius'] = 20;
			break;
		case 'left':
			style['borderTopLeftRadius'] = 20;
			style['borderBottomLeftRadius'] = 20;
			break;
		case 'right':
			style['borderTopRightRadius'] = 20;
			style['borderBottomRightRadius'] = 20;

			break;
		default:
			style['borderRadius'] = 4;
			break;
	}

	return style;
};
const styles = StyleSheet.create({

	buttonStyle: (theme, disabled, buttonColor, curved) => ({
		...getButton(theme, disabled, buttonColor, curved),

	}),
	buttonTitle: (theme) => ({
		color: theme.colors.white,
		alignSelf: 'center',
		fontFamily: 'Roboto-Bold',
	}),
});

Button.propTypes = {
	onPress: PropTypes.func.isRequired,
	children: PropTypes.string.isRequired,
	style: ViewPropTypes.style,
	disabled: PropTypes.bool,
};

Button.defaultProps = {
	style: {},
	disabled: false,
};

export {Button};
