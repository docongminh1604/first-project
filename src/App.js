import AsyncStorage from '@react-native-community/async-storage';
import React, {Component} from 'react';
import {
	Alert, 
	AppRegistry, 
	Linking,
	View
} from 'react-native';
import {store} from './store';
import {connect, Provider} from 'react-redux';
import {onAppStart} from './helper/app';
import {Navigator} from './navigation/Navigator';
import NavigationService from './navigation/NavigationService';
import {theme, ThemeProvider} from './theme';

onAppStart(store);

export default class DemoLayout extends Component {

	componentDidMount() {

	}
	render() {
		return (
			<Provider store={store}>
				<ThemeProvider theme={theme}>
					<Navigator
						ref={(navigatorRef) => {
							NavigationService.setTopLevelNavigator(navigatorRef);
						}}
						// uriPrefix={prefix}
					/>
				</ThemeProvider>
			</Provider>
		);
	}
}

const mapStateToProps = () => {
	return {};
};

const DemoLayoutConnected = connect(mapStateToProps, {

})(DemoLayout);

AppRegistry.registerComponent('DemoLayout', () => DemoLayoutConnected);