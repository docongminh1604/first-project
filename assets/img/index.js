const Images = {
  
  ic_bottom_account: require('./icon/ic_bottom_myaccount.png'),
  ic_bottom_category: require('./icon/ic_bottom_category.png'),
  ic_bottom_home: require('./icon/ic_bottom_home.png'),
  ic_bottom_home1: require('./icon/ic_bottom_home1.png'),
  ic_bottom_notifycation: require('./icon/ic_bottom_notification.png'),
  ic_bottom_setting: require('./icon/ic_bottom_setting.png'),

  ic_header_back: require('./icon/ic_header_back.png'),
  ic_header_menu: require('./icon/ic_header_menu.png'),
  ic_header_search: require('./icon/ic_search.png'),

  ic_logo: require('./icon/ic_my_order.png'),
  ic_search: require('./icon/ic_search.png'),
  ic_telephone: require('./icon/ic_telephone.png'),
  ic_user: require('./icon/ic_user.png'),
  ic_office: require('./icon/ic_Office.jpg'),

  ic_call: require('./icon/ic_call.png'),
  ic_credit_card: require('./icon/ic_card.png'),
  ic_checkBox_check: require('./icon/ic_check_box_check.png'),
  ic_checkBox_unCheck: require('./icon/ic_check_box_uncheck.png'),
  ic_close: require('./icon/ic_close.png'),
  ic_delete: require('./icon/ic_delete.png'),
  ic_down: require('./icon/ic_down_arrow.png'),
  ic_edit: require('./icon/ic_edit.png'),
  ic_filter: require('./icon/ic_filter.png'),
  ic_lock: require('./icon/ic_lock.png'),
  ic_logout: require('./icon/ic_logout.png'),
  ic_mail: require('./icon/ic_mail.png'),
  ic_news: require('./icon/ic_newspaper.png'),
  ic_plus_red: require('./icon/ic_plus_circle_red.png'),
  ic_reload: require('./icon/ic_reload.png'),
  ic_right: require('./icon/ic_right.png'),
  ic_share: require('./icon/ic_share.png'),
  ic_translation: require('./icon/ic_translation.png'),
  ic_heart_fill: require('./icon/ic_heart_fill.png'),
  ic_heart_empty: require('./icon/ic_heart_empty.png')

};
export default Images;
